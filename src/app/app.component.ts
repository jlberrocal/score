import { Component, OnDestroy } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { WebServer, Response } from '@ionic-native/web-server/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Subscription } from 'rxjs';
import {ScoreService} from './services/score.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy {
  private connectSubscription: Subscription;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private webServer: WebServer,
    private network: Network,
    private score: ScoreService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.platform.is('android')) {
        this.webServer.onRequest().subscribe(req => {
          const response: Response = {
            status: 200,
            headers: {
              'Content-Type': 'application/json'
            }
          };

          this.score.add('player1');
          this.webServer
            .sendResponse(req.requestId, response)
            .catch((error: any) => console.error(error));
        });
        this.webServer.start(3030);

        this.connectSubscription = this.network.onConnect().subscribe(() => {
          console.log('network connected!');
          // We just got a connection but we need to wait briefly
           // before we determine the connection type. Might need to wait.
          // prior to doing any api requests as well.
          setTimeout(() => {
            if (this.network.type === 'wifi') {
              console.log('we got a wifi connection, woohoo!');
            }
          }, 3000);
        });
      }
    });
  }

  ngOnDestroy() {
    this.connectSubscription.unsubscribe();
  }
}
