import { Component } from '@angular/core';
import {Observable} from 'rxjs';
import {Score} from '../models/score';
import {ScoreService} from '../services/score.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  score$: Observable<Score>;
  constructor(private service: ScoreService) {
    this.score$ = this.service.getScore();
  }

  add(player: keyof Score) {
    this.service.add(player);
  }

  reset() {
    this.service.reset();
  }
}
