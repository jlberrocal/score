import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Score} from '../models/score';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  private scoreSubject = new BehaviorSubject<Score>({player1: 0, player2: 0});

  constructor() {
  }

  getScore(): Observable<Score> {
    return this.scoreSubject.asObservable();
  }

  add(player: keyof Score) {
    const {value} = this.scoreSubject;
    value[player]++;
    this.scoreSubject.next(value);
  }

  reset() {
    this.scoreSubject.next({
      player1: 0,
      player2: 0
    });
  }
}
